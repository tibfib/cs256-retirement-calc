import React, {Component} from 'react';
import SimpleSlider from './SimpleSlider/SimpleSlider.js';

import './App.css';

class App extends Component {
	render() {
		return (
			<div className="App">
				<div className="App-header">
					My Retirement Planner
				</div>
                <div className="App-content">
                    <SimpleSlider />
                </div>
			</div>
		);
	}
}

export default App;
