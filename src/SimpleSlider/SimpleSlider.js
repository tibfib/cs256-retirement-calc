import React, {Component} from 'react';
import Slider from 'react-slick';

import 'slick-carousel/slick/slick-theme.css';
import 'slick-carousel/slick/slick.css';
import './SimpleSlider.css';

import Calculator from '../Calculator';

class SimpleSlider extends Component {
	constructor(props) {
		super(props);
		this.state = {
			age: 20,
			gross_monthly_income: 2000,
			amount_already_saved: 0
		};
	}
	handleChange(event, property) {
		this.setState({
			[property]: + event.target.value
		});
	}
	getResults(state) {
		return Calculator.getResults(state);
	}
	render() {
		var settings = {
			dots: true,
			infinte: false,
			fade: false,
			swipe: false
		};
		return (
			<div className="simple-slider">
				<Slider {...settings}>
					<div>
						<h2>How old are you?</h2>
						<input className="large" type="number" value={this.state.age} onChange={e => this.handleChange(e, 'age')} />
					</div>
					<div>
						<h2>How much do you make per month?</h2>
						<input className="medium" value={this.state.gross_monthly_income} onChange={e => this.handleChange(e, 'gross_monthly_income')} />
						<div className="info-text">
							Think of the amount <em>after</em> taxes.
						</div>
					</div>
					<div>
						<h2>How much have you already saved?</h2>
						<input className="small" value={this.state.amount_already_saved} onChange={e => this.handleChange(e, 'amount_already_saved')} />
						<div className="info-text">How much is in your retirement fund right now?</div>
					</div>
					<div>
						<Results results={this.getResults(this.state)}/>
					</div>
				</Slider>
			</div>
		);
	}
};


class Results extends Component {

	render() {
		return (
			<div className="results">
				<p className="big-paragraph">
					Okay, in order to maintain your monthly income of <strong>{this.props.results.gross_monthly_income}</strong>, you'll need to deposit <strong>{this.props.results.monthly_deposit_needed}</strong> every month to your retirement fund.
				</p>

				<details>
					<summary>Curious about the details?</summary>
					<div>
						<p>Here's how it works. We assumed a 7% return on investment and a monthly social security payout of {this.props.results.social_security_payout}. Also, we assumed that you would retire at age 62. Those are all the retirement averages.</p>

						<p>With those values in mind we were able to calculate the following:</p>
						<ul>
							<li>
								The amount you've <em>already</em> saved will be worth this much when you retire: <strong>{this.props.results.current_savings_at_retirement}</strong>
								</li>
							<li>
								Assuming a payout of {this.props.results.social_security_payout} each month in retirement, you will need {this.props.results.gross_monthly_income} - {this.props.results.social_security_payout} = <strong>{this.props.results.monthly_money_needed_in_retirement}</strong> per month.
							</li>
							<li>
								Assuming 300 months of retirement, the total amount you'll need is <strong>{this.props.results.total_money_needed}</strong>
							</li>
							<li>
								With a 7% return on investment, a monthly deposit of <strong>{this.props.results.monthly_deposit_needed}</strong> until you retire will give you the amount you need.
							</li>
						 </ul>
					</div>
				</details>
			</div>
		)
	}
}

export default SimpleSlider;
