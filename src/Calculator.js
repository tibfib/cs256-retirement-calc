let service = {};

const SOCIAL_SECURITY_MONTHLY_PAYOUT = 1180.80; // avg in June 2011
const RETURN_ON_INVESTMENT = 0.07; // avg return
const AVG_RETIREMENT_AGE = 62; // avg

function formatCurrency(value) {
	return '$' + value.toFixed(2);
}

function getFutureDollarValue(years) {
	return 1 * Math.pow(1 + (RETURN_ON_INVESTMENT / 100), years);
}

const MONTHLY_INTEREST_RATE = RETURN_ON_INVESTMENT/12;
function getDepositNecessary(total_money_needed, years_to_retirement) {
	let num_payments = years_to_retirement * 12;
	let future_value_of_dollar = (Math.pow(1 + MONTHLY_INTEREST_RATE, num_payments) - 1) / MONTHLY_INTEREST_RATE;
	return total_money_needed / future_value_of_dollar;
}

service.getResults = ({age, gross_monthly_income, amount_already_saved}) => {
	let years_to_retirement = AVG_RETIREMENT_AGE - age;

	let current_savings_at_retirement = amount_already_saved * getFutureDollarValue(years_to_retirement);

	let total_money_needed = (300 * (gross_monthly_income - SOCIAL_SECURITY_MONTHLY_PAYOUT));
	let money_still_needed = total_money_needed - current_savings_at_retirement;

	return {
		current_savings_at_retirement: formatCurrency(amount_already_saved * getFutureDollarValue(years_to_retirement)),
		monthly_deposit_needed: formatCurrency(getDepositNecessary(money_still_needed, years_to_retirement)),
		total_money_needed: formatCurrency(total_money_needed),
		gross_monthly_income: formatCurrency(gross_monthly_income),
		social_security_payout: formatCurrency(SOCIAL_SECURITY_MONTHLY_PAYOUT),
		monthly_money_needed_in_retirement: formatCurrency(gross_monthly_income - SOCIAL_SECURITY_MONTHLY_PAYOUT)
	};
}

export default service;
